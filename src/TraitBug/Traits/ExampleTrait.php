<?php

namespace TraitBug\Traits;

trait ExampleTrait {
  public function exampleFunction() : string {
    return 'Example function called';
  }
}
