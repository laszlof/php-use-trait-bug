<?php

namespace TraitBug\Traits;

trait TestTrait {
  public function testFunction() : string {
    return 'Test function called';
  }
}
