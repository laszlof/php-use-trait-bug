<?php

namespace TraitBug;

use TraitBug\Traits\ExampleTrait;
use TraitBug\Traits\TestTrait;

class Test {
  use ExampleTrait, TestTrait;

  public function runTest() : void {
    echo "TEST OUTPUT: {$this->testFunction()}\n";
    echo "EXAMPLE OUTPUT: {$this->exampleFunction()}\n";
  }
}
